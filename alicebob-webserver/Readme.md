There are two playbooks in this directory 

**webclient.yml**   
A two node web server and client experiment.   
*Node Bob*: configured as an Apache2 server.   
*Node Alice*: configured as a web client fetching one page and then quitting.


**webclient-withpages.yml**    
A two node web server and client experiment.   
*Node Bob*: configured as an Apache2 server with a range of page sizes 
*Node Alice*: configured as a web client fetching a random size page ten times from the server and then quitting. 
This playbook uses nested loops and includes `innerloop.yml`

 
