#!/bin/bash

set -e
set -x

#mergetb new exp mpl --src=model.py
mergetb login $user --passwd $passwd
mergetb realize mpl one --accept
mergetb materialize mpl one
mergetb wait mpl one
attach $user mpl one

# XXX hack until merge includes foundry readiness in completion condition
until ping -c1 a >/dev/null 2>&1; do :; done
until ping -c1 b >/dev/null 2>&1; do :; done
until ping -c1 c >/dev/null 2>&1; do :; done

ansible-playbook -i hosts run.yml

