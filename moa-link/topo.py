import mergexp as mx
from mergexp.net import capacity, latency
from mergexp.unit import mbps, ms

net = mx.Topology('moa-link')

a = net.device('a')
b = net.device('b')
link = net.connect([a, b], capacity == mbps(10), latency == ms(20))

link[a].ip.addrs = ['10.0.0.1/24']
link[b].ip.addrs = ['10.0.0.2/24']

mx.experiment(net)
