import mergexp as mx
from mergexp.net import capacity, latency
from mergexp.unit import mbps, ms

net = mx.Topology('alicebob')

alice = net.device('alice')
bob = net.device('bob')
link = net.connect([alice, bob], capacity == mbps(10), latency == ms(20))

link[alice].ip.addrs = ['10.0.0.1/24']
link[bob].ip.addrs = ['10.0.0.2/24']

mx.experiment(net)
